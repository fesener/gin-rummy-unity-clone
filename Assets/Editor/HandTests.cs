﻿using NUnit.Framework;
using System.Collections.Generic;

public class HandTests
{
    /*[Test]
    public void ShouldReturn10CardsAsStartingHand()
    {
        Deck testDeck = new Deck();
        testDeck.Shuffle();
        Hand testHand = new Hand();
        testHand.DrawStartingHand();
       Assert.AreEqual(testHand.cards.Count, 10);
    }*/

    [Test]
    public void ShouldSortBySequencesAndTakeSequencesToTheBeginningBySuitOrder()
    {
        Hand testHand = new Hand();

         Card card1 = new Card(Suit.Hearts, Value.Ace); // Ace of Hearts
         Card card2 = new Card(1); // Two of Spades
         Card card3 = new Card(30); // Five of Diamonds
         Card card4 = new Card(16); // Four of Hearts
         Card card5 = new Card(Suit.Spades, Value.Ace); // Ace of Spades
         Card card6 = new Card(Suit.Diamonds, Value.Three); // Three of Diamonds
         Card card7 = new Card(Suit.Clubs, Value.Four); // Four of Clubs
         Card card8 = new Card(Suit.Spades, Value.Four); // Four of Spades
         Card card9 = new Card(Suit.Diamonds, Value.Ace); // Ace of Diamonds
         Card card10 = new Card(Suit.Spades, Value.Three); // Three of Spades
         Card card11 = new Card(Suit.Diamonds, Value.Four); // Four of Diamonds



         // Test Hand

         testHand.AddCard(card1);
         testHand.AddCard(card2);
         testHand.AddCard(card3);
         testHand.AddCard(card4);
         testHand.AddCard(card5);
         testHand.AddCard(card6);
         testHand.AddCard(card7);
         testHand.AddCard(card8);
         testHand.AddCard(card9);
         testHand.AddCard(card10);
         testHand.AddCard(card11);



        // Expected Result
        Hand testHand_expected = new Hand();
        testHand_expected.AddCard(card5); // Ace of Spades
        testHand_expected.AddCard(card2); // Two of Spades
        testHand_expected.AddCard(card10); // Three of Spades
        testHand_expected.AddCard(card8); // Four of Spades
        testHand_expected.AddCard(card6); // Three of Diamonds
        testHand_expected.AddCard(card11); // Four of Diamonds
        testHand_expected.AddCard(card3); // Five of Diamonds
        testHand_expected.AddCard(card1); // Ace of Hearts
        testHand_expected.AddCard(card4); // Four of Hearts
        testHand_expected.AddCard(card9); // Ace of Diamonds
        testHand_expected.AddCard(card7); // Four of Clubs


        HandGroup hg = testHand.SortByCompleteSequences(testHand.cards);
        List<Card> result = hg.sorted;
        result.AddRange(hg.unsorted);

        CollectionAssert.AreEqual(result, testHand_expected.cards);
    }

    [Test]
    public void ShouldCalculate7WhenGivenBrokenSequenceWithAceTwoFour()
    {
        Hand testHand = new Hand();

        Card ca1 = new Card(0);
        Card ca2 = new Card(1);
        Card ca3 = new Card(2);
        Card ca4 = new Card(3);
        CardGroup cg = new CardGroup(GroupType.CompleteSequence);
        cg.AddCard(ca1);
        cg.AddCard(ca2);
        cg.AddCard(ca3);
        cg.AddCard(ca4);

        int value = testHand.CalculateBrokenSequence(cg, ca3);

        Assert.AreEqual(value, 7);
    }

    [Test]
    public void ShouldCalculate12WhenGivenBrokenSequenceWithAceTwoFourFive()
    {
        Hand testHand = new Hand();

        Card ca1 = new Card(0);
        Card ca2 = new Card(1);
        Card ca3 = new Card(2);
        Card ca4 = new Card(3);
        Card ca5 = new Card(4);
        CardGroup cg = new CardGroup(GroupType.CompleteSequence);
        cg.AddCard(ca1);
        cg.AddCard(ca2);
        cg.AddCard(ca3);
        cg.AddCard(ca4);
        cg.AddCard(ca5);

        int value = testHand.CalculateBrokenSequence(cg, ca3);

        Assert.AreEqual(value, 12);
    }

    [Test]
    public void ShouldCalculate3WhenGivenBrokenSequenceWithAceTwoFourFiveSix()
    {
        Hand testHand = new Hand();

        Card ca1 = new Card(0);
        Card ca2 = new Card(1);
        Card ca3 = new Card(2);
        Card ca4 = new Card(3);
        Card ca5 = new Card(4);
        Card ca6 = new Card(5);

        CardGroup cg = new CardGroup(GroupType.CompleteSequence);
        cg.AddCard(ca1);
        cg.AddCard(ca2);
        cg.AddCard(ca3);
        cg.AddCard(ca4);
        cg.AddCard(ca5);
        cg.AddCard(ca6);

        int value = testHand.CalculateBrokenSequence(cg, ca3);


        Assert.AreEqual(value, 3);
    }

    [Test]
    public void ShouldCalculate6WhenGivenBrokenSequenceWithAceTwoThreeFourSix()
    {

        Hand testHand = new Hand();

        Card ca1 = new Card(0);
        Card ca2 = new Card(1);
        Card ca3 = new Card(2);
        Card ca4 = new Card(3);
        Card ca5 = new Card(4);
        Card ca6 = new Card(5);

        CardGroup cg = new CardGroup(GroupType.CompleteSequence);
        cg.AddCard(ca1);
        cg.AddCard(ca2);
        cg.AddCard(ca3);
        cg.AddCard(ca4);
        cg.AddCard(ca5);
        cg.AddCard(ca6);

        int value = testHand.CalculateBrokenSequence(cg, ca5);


        Assert.AreEqual(value, 6);
    }


    [Test]
    public void ShouldSortByGroupsAndTakeGroupsToTheBeginningBySuitOrder()
    {
        Hand testHand = new Hand();

        Card card1 = new Card(Suit.Hearts, Value.Ace); // Ace of Hearts
        Card card2 = new Card(1); // Two of Spades
        Card card3 = new Card(30); // Five of Diamonds
        Card card4 = new Card(16); // Four of Hearts
        Card card5 = new Card(Suit.Spades, Value.Ace); // Ace of Spades
        Card card6 = new Card(Suit.Diamonds, Value.Three); // Three of Diamonds
        Card card7 = new Card(Suit.Clubs, Value.Four); // Four of Clubs
        Card card8 = new Card(Suit.Spades, Value.Four); // Four of Spades
        Card card9 = new Card(Suit.Diamonds, Value.Ace); // Ace of Diamonds
        Card card10 = new Card(Suit.Spades, Value.Three); // Three of Spades
        Card card11 = new Card(Suit.Diamonds, Value.Four); // Four of Diamonds


        // Test Hand

        testHand.AddCard(card1);
        testHand.AddCard(card2);
        testHand.AddCard(card3);
        testHand.AddCard(card4);
        testHand.AddCard(card5);
        testHand.AddCard(card6);
        testHand.AddCard(card7);
        testHand.AddCard(card8);
        testHand.AddCard(card9);
        testHand.AddCard(card10);
        testHand.AddCard(card11);

        // Expected Result
        Hand testHand_expected = new Hand();
        testHand_expected.AddCard(card5); // Ace of Spades
        testHand_expected.AddCard(card1); // Ace of Hearts
        testHand_expected.AddCard(card9); // Ace of Diamonds
        testHand_expected.AddCard(card8); // Four of Spades
        testHand_expected.AddCard(card4); // Four of Hearts
        testHand_expected.AddCard(card11); // Four of Diamonds
        testHand_expected.AddCard(card7); // Four of Clubs
        testHand_expected.AddCard(card2); // Two of Spades
        testHand_expected.AddCard(card10); // Three of Spades
        testHand_expected.AddCard(card6); // Three of Diamonds
        testHand_expected.AddCard(card3); // Five of Diamonds
 

        HandGroup hg = testHand.SortByCompleteGroups(testHand.cards);
        List<Card> sortedList = hg.sorted;
        sortedList.AddRange(hg.unsorted);
        CollectionAssert.AreEqual(sortedList, testHand_expected.cards);
    }

    [Test]
    public void ShouldSortSmart()
    {
        Hand testHand = new Hand();

        Card card1 = new Card(Suit.Hearts, Value.Ace); // Ace of Hearts
        Card card2 = new Card(1); // Two of Spades
        Card card3 = new Card(30); // Five of Diamonds
        Card card4 = new Card(16); // Four of Hearts
        Card card5 = new Card(Suit.Spades, Value.Ace); // Ace of Spades
        Card card6 = new Card(Suit.Diamonds, Value.Three); // Three of Diamonds
        Card card7 = new Card(Suit.Clubs, Value.Four); // Four of Clubs
        Card card8 = new Card(Suit.Spades, Value.Four); // Four of Spades
        Card card9 = new Card(Suit.Diamonds, Value.Ace); // Ace of Diamonds
        Card card10 = new Card(Suit.Spades, Value.Three); // Three of Spades
        Card card11 = new Card(Suit.Diamonds, Value.Four); // Four of Diamonds


        // Test Hand

        testHand.AddCard(card1);
        testHand.AddCard(card2);
        testHand.AddCard(card3);
        testHand.AddCard(card4);
        testHand.AddCard(card5);
        testHand.AddCard(card6);
        testHand.AddCard(card7);
        testHand.AddCard(card8);
        testHand.AddCard(card9);
        testHand.AddCard(card10);
        testHand.AddCard(card11);

        // Expected Result
        Hand testHand_expected = new Hand();
        testHand_expected.AddCard(card5); // Ace of Spades
        testHand_expected.AddCard(card2); // Two of Spades
        testHand_expected.AddCard(card10); // Three of Spades

        testHand_expected.AddCard(card6); // Three of Diamonds
        testHand_expected.AddCard(card11); // Four of Diamonds
        testHand_expected.AddCard(card3); // Five of Diamonds

        testHand_expected.AddCard(card4); // Four of Hearts
        testHand_expected.AddCard(card7); // Four of Clubs
        testHand_expected.AddCard(card8); // Four of Spades

        testHand_expected.AddCard(card1); // Ace of Hearts
        testHand_expected.AddCard(card9); // Ace of Diamonds

        HandGroup hg = testHand.SmartSort(testHand.cards);
        List<Card> result = hg.sorted;
        CollectionAssert.AreEqual(result, testHand_expected.cards);
    }

}