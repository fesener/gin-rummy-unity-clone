﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System;

public class CardTests {

    // Test by number parameter
    [Test]
    public void ShouldReturnFiveWhenGiven43()
    {
        Card testCardByNumber = new Card(43);
        Assert.AreEqual(testCardByNumber.Value, Value.Five);
    }

    [Test]
    public void ShouldReturnQueenWhenGiven24()
    {
        Card testCardByNumber = new Card(24);
        Assert.AreEqual(testCardByNumber.Value, Value.Queen);
    }

    [Test]
    public void ShouldReturnHeartsWhenGiven17()
    {
        Card testCardByNumber = new Card(17);
        Assert.AreEqual(testCardByNumber.Suit, Suit.Hearts);
    }

    [Test]
    public void ShouldReturnClubsWhenGiven48()
    {
        Card testCardByNumber = new Card(48);
        Assert.AreEqual(testCardByNumber.Suit, Suit.Clubs);
    }

    [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void ShouldReturnNullWhenGivenNegativeNumber()
    {
        Card testCardByNumber = new Card(-1);
        Assert.IsNull(testCardByNumber);
    }

    [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
    public void ShouldReturnNullWhenGivenNumberBiggerThan51()
    {
        Card testCardByNumber = new Card(52);
        Assert.IsNull(testCardByNumber);
    }

    // Test by suit and value parameters

    [Test]
    public void ShouldReturnAceOfSpadesWhenGivenAceandSpades()
    {
        Card testCardBySuitAndValue = new Card(Suit.Spades, Value.Ace);
        Assert.AreEqual(testCardBySuitAndValue.Suit, Suit.Spades);
        Assert.AreEqual(testCardBySuitAndValue.Value, Value.Ace);
    }

    [Test]
    public void ShouldReturnQueenOfHeartsWhenGivenQueenAndHearts()
    {
        Card testCardBySuitAndValue = new Card(Suit.Hearts, Value.Queen);
        Assert.AreEqual(testCardBySuitAndValue.Suit, Suit.Hearts);
        Assert.AreEqual(testCardBySuitAndValue.Value, Value.Queen);
    }

    [Test]
    public void ShouldReturnEqualWhenSameCardCreatedByTwoDifferentMethods()
    {
        Card testCardByNumber = new Card(1);
        Card testCardBySuitAndValue = new Card(Suit.Spades, Value.Two);
        Assert.AreEqual(testCardByNumber.Value, testCardBySuitAndValue.Value);
        Assert.AreEqual(testCardByNumber.Suit, testCardBySuitAndValue.Suit);
    }
}
