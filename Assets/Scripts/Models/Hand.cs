﻿using System.Collections.Generic;
using System;

public class Hand {

    public const int maxHandSize = 11;

    public int startingHandSize = 10;

    int currentHandSize;

    public List<Card> cards { get; set; }

    public delegate void HandCardChangedHandler(Card c);
    public event HandCardChangedHandler HandCardAdded;

    public Hand()
    {
        cards =  new List<Card>();
        currentHandSize = 0;
    }

    public void AddCard(Card card)
    {
        if (currentHandSize >= maxHandSize || card == null) return;
        cards.Add(card);
        currentHandSize++;
        if(HandCardAdded != null) HandCardAdded(card);
    }


    public void DiscardCard(Card card)
    {
        if (cards.Contains(card))
        {
            cards.Remove(card);
            currentHandSize--;
        } else
        {
            throw new KeyNotFoundException();
        }
    }

    private List<Card> sortForSet(List<Card> cardList)
    {
        Comparison<Card> valueOrder = (c1, c2) =>
        {
            int result = c1.Value.CompareTo(c2.Value);
            return result == 0 ? c1.Suit.CompareTo(c2.Suit) : result;
        };
        
        cardList.Sort(valueOrder);
        return cardList;
    }

    private List<Card> sortForRun(List<Card> cardList)
    {
        Comparison<Card> suitToValue = (c1, c2) =>
        {
            int result = c1.Suit.CompareTo(c2.Suit);
            return result == 0 ? c1.Value.CompareTo(c2.Value) : result;
        };
        cardList.Sort(suitToValue);
        return cardList;
    }

    public HandGroup SortByCompleteSequences(List<Card> cardList)
    {
        cardList = sortForRun(cardList);
        List<Card> sequences = new List<Card>();
        List<Card> nonSequences = new List<Card>();
        List<CardGroup> sequenceGroups = new List<CardGroup>();
        int count = 0;
        int startingIndex = 0;
        for (int i = 0; i < cardList.Count; i++)
        {
            if (count == 0)
            {
                startingIndex = i;
                count = 1;
                nonSequences.Add(cardList[i]);
            }
            else if (cardList[i].Value == cardList[startingIndex].Value + count && cardList[i].Suit == cardList[startingIndex + count].Suit && cardList[startingIndex + count] != null)
            {
                count++;
                nonSequences.Add(cardList[i]);
                if (count == 3)
                {
                    CardGroup group = new CardGroup(GroupType.CompleteSequence);
                    for (int j = startingIndex; j < startingIndex + count; j++)
                    {
                        sequences.Add(cardList[j]);
                        group.AddCard(cardList[j]);

                        if (nonSequences.Contains(cardList[j])) nonSequences.Remove(cardList[j]);
                    }
                    sequenceGroups.Add(group);
                }
                else if (count > 3)
                {
                    CardGroup group = new CardGroup(GroupType.CompleteSequence);
                    group = sequenceGroups[sequenceGroups.Count -1 ];
                    group.AddCard(cardList[i]);
                    sequences.Add(cardList[i]);
                    if (nonSequences.Contains(cardList[i])) nonSequences.Remove(cardList[i]);
                }
            }
            else
            {
                nonSequences.Add(cardList[i]);
                count = 1;
                startingIndex = i;
            }
        }
        HandGroup handGroup = new HandGroup(sequences, nonSequences, sequenceGroups);
        return handGroup;
    }


    public HandGroup SortByCompleteGroups(List<Card> cardList)
    {
        cardList = sortForSet(cardList);
        List<Card> groups = new List<Card>();
        List<Card> nonGroups = new List<Card>();
        List<CardGroup> groupCardGroups = new List<CardGroup>();
        int count = 0;
        int startingIndex = 0;
        for (int i = 0; i < cardList.Count; i++)
        {
            if (count == 0)
            {
                startingIndex = i;
                count = 1;
                nonGroups.Add(cardList[i]);
            }
            else if (cardList[i].Suit == cardList[startingIndex].Suit + count && cardList[i].Value == cardList[startingIndex + count].Value && cardList[startingIndex + count] != null)
            {
                count++;
                nonGroups.Add(cardList[i]);
                if (count == 3)
                {
                    CardGroup group = new CardGroup(GroupType.CompleteGroup);
                    for (int j = startingIndex; j < startingIndex + count; j++)
                    {
                        groups.Add(cardList[j]);
                        group.AddCard(cardList[j]);

                        if (nonGroups.Contains(cardList[j])) nonGroups.Remove(cardList[j]);
                    }
                    groupCardGroups.Add(group);
                }
                else if (count > 3)
                {
                    CardGroup group = new CardGroup(GroupType.CompleteGroup);
                    group = groupCardGroups[groupCardGroups.Count - 1];
                    group.AddCard(cardList[i]);
                    groups.Add(cardList[i]);
                    if (nonGroups.Contains(cardList[i])) nonGroups.Remove(cardList[i]);
                }
            }
            else
            {
                nonGroups.Add(cardList[i]);
                count = 1;
                startingIndex = i;
            }
        }
        HandGroup handGroup = new HandGroup(groups, nonGroups, groupCardGroups);
        return handGroup;
    }



    public HandGroup SmartSort(List<Card> listOfCards)
    {
        List<Card> cardList = new List<Card>();
        cardList.AddRange(listOfCards);

        // Find all complete sequences in the list
        HandGroup completeSequences = SortByCompleteSequences(cardList);
        // Add the sequences we found to a CardGroup list to evaluate later with partial groups we will find
        List<CardGroup> completeSequenceGroups = completeSequences.cardGroup;
        // Find all complete groups from the remaining of the list that are not sorted yet 
        HandGroup completeGroups = SortByCompleteGroups(completeSequences.unsorted);
        // Add the partial groups we found  to a CardGroup list to evaluate later with complete sequences we found earlier
        List<CardGroup> partialGroups = FindPartialGroupsOrderedByRank(completeGroups.unsorted);

        // Add all complete sequences and groups we found so far the the list
        cardList = completeSequences.sorted;
        cardList.AddRange(completeGroups.sorted);

        // Check if a partial groups exists which can become a complete group by getting a card from the existing complete sequences
        foreach (CardGroup sequence in completeSequenceGroups)   
        {
            foreach (CardGroup partialGroup in partialGroups)
            {
                // Check if sequence contains a card with the same value as our partial group
                if(sequence.ContainsValue(partialGroup.GetCardValue()))
                {
                    // If found, assign that card to a variable
                    Card cardToSwap = sequence.FindCardWithValue(partialGroup.GetCardValue());
                    
                    // Check if the total value of the hand increases if we take the card from complete sequence and add it to the partial group
                    if (partialGroup.GetRank() > CalculateBrokenSequence(sequence, cardToSwap))//sequence.GetRank() - partialGroup.GetCardValue())
                    {
                        // Remove card from current ordered card list
                        cardList.Remove(cardToSwap);
                        // Add card to the partial group we found which gives better value when the card added
                        partialGroup.AddCard(cardToSwap);
                        // Add the whole partial group (which is a complete group now) to our sorted card list
                        cardList.AddRange(partialGroup.group);
                        // Remove the complete group from the partial groups list
                        partialGroups.Remove(partialGroup);

                        break;
                    } 
                }
            }
        }

        // Add all the remaining cards which does not belong to a complete sequence or group
        foreach(Card c in listOfCards)
            if (!cardList.Contains(c))
            {
                cardList.Add(c);
            }

        HandGroup returnHand = new HandGroup(cardList, null);
        return returnHand;

    }

    // Function to calculate the value of a broken sequence (lower the better)
    public int CalculateBrokenSequence(CardGroup sequence, Card card)
    {
        List<Card> sequenceCards = new List<Card>();
        sequenceCards.AddRange(sequence.group);
        sequenceCards.Remove(card);

        int value = 0;
        int count = 0;
        int startingIndex = 0;
        for (int i = 0; i < sequenceCards.Count; i++)
        {
            if (count == 0)
            {
                startingIndex = i;
                count = 1;
                value += (int)sequenceCards[i].Value;
            } else if(count == 1)
            {
                startingIndex = i - 1;
                count++;
                value += (int)sequenceCards[i].Value;
            }
            else if (sequenceCards[i].Value == sequenceCards[startingIndex].Value + count)
            {
                count++;
                if (count == 3)
                {
                    value += (int)sequenceCards[i].Value;
                    for (int j = startingIndex; j < startingIndex + count; j++)
                    {
                        value -= (int)sequenceCards[j].Value;
                    }
                } 

            }
            else
            {
                value += (int)sequenceCards[i].Value;
                count = 1;
            }


        }

        return value;
    }

    public List<CardGroup> FindCompleteSequences(List<Card> cardList, out List<Card> remaining)
    {
        cardList = sortForRun(cardList);
        List<CardGroup> sequences = new List<CardGroup>();
        remaining = new List<Card>();
        int count = 0;
        int startingIndex = 0;
        for (int i = 0; i < cardList.Count; i++)
        {
            if (count == 0)
            {
                startingIndex = i;
                count = 1;
                remaining.Add(cardList[i]);
            }
            else if (cardList[i].Value == cardList[startingIndex].Value + count && cardList[i].Suit == cardList[startingIndex + count].Suit && cardList[startingIndex + count] != null)
            {
                count++;
                if (count >= 3)
                {
                    CardGroup group = new CardGroup(GroupType.CompleteSequence);
                    for (int j = startingIndex; j < startingIndex + count; j++)
                    {
                        group.AddCard(cardList[j]);
                        if (remaining.Contains(cardList[j]))
                        {
                            remaining.Remove(cardList[j]);
                        }
                    }
                    sequences.Add(group);
                    
                }
            }
            else
            {
                remaining.Add(cardList[i]);
                count = 0;
            }
        }
        return sequences;
    }


    public List<CardGroup> FindCompleteGroups(List<Card> cardList)
    {
        cardList = sortForSet(cardList);
        List<CardGroup> groups = new List<CardGroup>();

        int count = 0;
        int startingIndex = 0;
        for (int i = 0; i < cardList.Count; i++)
        {
            if (count == 0)
            {
                startingIndex = i;
                count = 1;
            }
            else if (cardList[i].Suit == cardList[startingIndex].Suit + count && cardList[i].Value == cardList[startingIndex + count].Value && cardList[startingIndex + count] != null)
            {
                count++;
            }
            else
            {
                if (count >= 3)
                {
                    CardGroup group = new CardGroup(GroupType.CompleteGroup);
                    for (int j = startingIndex; j < startingIndex + count; j++)
                    {
                        group.AddCard(cardList[j]);
                    }
                    groups.Add(group);
                }
                count = 0;
            }
        }
        return groups;
    }

    public List<CardGroup> FindPartialGroupsOrderedByRank(List<Card> cardList)
    {
        cardList = sortForSet(cardList);
        List<CardGroup> partialGroups = new List<CardGroup>();

        for (int i = 0; i < cardList.Count; i++)
        {
            for (int j = i+1, length = Math.Min(i + 4, cardList.Count); j < length; j++)
            {
                if (cardList[j] != null && cardList[i].Suit != cardList[j].Suit && cardList[i].Value == cardList[j].Value)
                {
                    CardGroup group = new CardGroup(GroupType.PartialGroup);
                    group.AddCard(cardList[i]);
                    group.AddCard(cardList[j]);
                    partialGroups.Add(group);
                }
            }
        }

        partialGroups.Sort();
        return partialGroups;
    }

    /*public List<CardGroup> FindPartialSequences()
    {
        sortForRun();
        List<CardGroup> partialSequences = new List<CardGroup>();

        for (int i = 0; i < cards.Count; i++)
        {
            for (int j = i + 1; j < cards.Count; j++)
            {
                if (cards[i].Suit == cards[j].Suit && cards[i].Value == cards[j].Value - j)
                {
                    if(cards[j].Equals(cards[i+1]))
                    CardGroup group = new CardGroup(GroupType.PartialGroup);
                    group.AddCard(cards[i]);
                    group.AddCard(cards[j]);
                    partialSequences.Add(group);
                }
            }
        }

        partialSequences.Sort();
        return partialSequences;
    }

    */

}

public class HandGroup
{
    public List<Card> sorted;
    public List<Card> unsorted;
    public List<CardGroup> cardGroup;

    public HandGroup(List<Card> sorted, List<Card> unsorted, List<CardGroup> cardGroup = null)
    {
        this.sorted = sorted;
        this.unsorted = unsorted;
        this.cardGroup = cardGroup;
    }


}

public class CardGroup : IComparable
{
    GroupType type;
    public List<Card> group { get; protected set; }

    int rank;

    public CardGroup(GroupType type)
    {
        this.type = type;
        group = new List<Card>();
    }

    public void AddCard(Card c)
    {
        group.Add(c);
        rank += Math.Min((int)c.Value, 10);
    }

    public void RemoveCard(Card c)
    {
        group.Remove(c);
        rank -= Math.Min((int)c.Value, 10);
    }

    public int GetRank()
    {
        return rank;
    }

    public int CompareTo(Object obj)
    {
        if(obj is CardGroup)
        {
            return this.rank.CompareTo((obj as CardGroup).rank) * -1;
        }
        else
        {
            return -1;
        }
    }

    public int GetCardValue()
    {
        if (type == GroupType.CompleteGroup || type == GroupType.PartialGroup)
            return (int)group[0].Value;
        else return -1;
    }

    public bool ContainsValue(int value)
    {
        if (type == GroupType.CompleteGroup || type == GroupType.PartialGroup)
        {
            return value == (int)group[0].Value;
        } else
        {
            foreach(Card c in group)
            {
                if (value == (int) c.Value) return true;
            }
            return false;
        }
    }   

    public bool Contains(CardGroup cg)
    {
        foreach(Card c in cg.group)
        {
            if (group.Contains(c)) return true;
        }
        return false;
    }

    public Card FindCardWithValue(int value)
    {
        if(type == GroupType.CompleteSequence || type == GroupType.PartialSequence)
        {
            foreach(Card c in group)
            {
                if((int)c.Value == value)
                {
                    return c;
                }
            }
        }
        return null;
    }
   
}
