﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
    private static SoundManager _instance;
    public static SoundManager Instance { get { return _instance; } }

    AudioSource audioSource;

    public AudioClip deckClicked;
    public AudioClip cardDrawn;
    public AudioClip cardSlide;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else {
            _instance = this;
        }
    }

    IEnumerator Start()
    {
        yield return null;
        InputManager.Instance.DeckClicked += this.OnDeckClicked;
        GameManager.Instance.deck.CardDrawn += this.OnCardDrawn;
        HandManager.Instance.CardMoved += this.OnCardMoved;
        InputManager.Instance.DragStarted += OnDragStarted;
        InputManager.Instance.DragEnded += OnDragEnded;

        audioSource = GetComponent<AudioSource>();
    }

    void OnDeckClicked()
    {

    }

    void OnCardDrawn()
    {
        audioSource.PlayOneShot(cardDrawn);
    }

    void OnCardMoved()
    {
        audioSource.PlayOneShot(cardSlide);
    }

    void OnDragStarted(GameObject cardGO)
    {
        audioSource.PlayOneShot(cardSlide);
    }

    void OnDragEnded(GameObject cardGO)
    {
        audioSource.PlayOneShot(cardSlide);
    }

    void OnDestroy()
    {
        InputManager.Instance.DeckClicked -= this.OnDeckClicked;
        GameManager.Instance.deck.CardDrawn -= this.OnCardDrawn;
        HandManager.Instance.CardMoved -= this.OnCardMoved;
        InputManager.Instance.DragStarted -= OnDragStarted;
        InputManager.Instance.DragEnded -= OnDragEnded;
    }
}
