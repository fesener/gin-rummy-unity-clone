﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {


    private static GameManager _gameManager;    
    public static GameManager Instance
    {
        get
        {
            return _gameManager;
        }
    }

    public GameObject particlePrefab;

 
    public Deck deck { get; protected set; }
    public Dictionary<Card, GameObject> CardGameObjectDict = new Dictionary<Card, GameObject>();
    public Dictionary<GameObject, Card> GameObjectCardDict = new Dictionary<GameObject, Card>();


    void Awake()
    {
        if (_gameManager != null && _gameManager != this)
        {
            Destroy(this.gameObject);
        }
        else {
            _gameManager = this;
        }
    }

    void Start()
    {
        deck = new Deck();
        CreateDeck();
    }

    public void ShuffleDeck()
    {
        deck.Shuffle();
    }

    public void LoadTestData()
    {

        Card H1 = deck.DrawSpecificCard(13);
        Card S2 = deck.DrawSpecificCard(1);
        Card D5 = deck.DrawSpecificCard(30);
        Card H4 = deck.DrawSpecificCard(16);
        Card S1 = deck.DrawSpecificCard(0);
        Card D3 = deck.DrawSpecificCard(28);
        Card C4 = deck.DrawSpecificCard(42);
        Card S4 = deck.DrawSpecificCard(3);
        Card D1 = deck.DrawSpecificCard(26);
        Card S3 = deck.DrawSpecificCard(2);
        Card D4 = deck.DrawSpecificCard(29);

        HandManager.Instance.AddCardToHand(H1);
        HandManager.Instance.AddCardToHand(S2);
        HandManager.Instance.AddCardToHand(D5);
        HandManager.Instance.AddCardToHand(H4);
        HandManager.Instance.AddCardToHand(S1);
        HandManager.Instance.AddCardToHand(D3);
        HandManager.Instance.AddCardToHand(C4);
        HandManager.Instance.AddCardToHand(S4);
        HandManager.Instance.AddCardToHand(D1);
        HandManager.Instance.AddCardToHand(S3);
        HandManager.Instance.AddCardToHand(D4);

    }


    // TODO: This needs to be in another manager (DeckController ?)
    public void CreateDeck()
    {
        GameObject cardGameObjects_container = new GameObject();
        cardGameObjects_container.AddComponent<BoxCollider>().size = new Vector3(CardMeshCreator.Instance.cardMeshWidth *2 , 4 , CardMeshCreator.Instance.cardMeshHeight*2 );
        cardGameObjects_container.name = "Cards Container";
        for (int i = 0; i < deck.cards.Count; i++)
        {
            GameObject cardParentObject = new GameObject();
            cardParentObject.transform.SetParent(cardGameObjects_container.transform);
            cardParentObject.name = deck.cards[i].ToString() + " Parent";
            cardParentObject.AddComponent<CardInfo>().card = deck.cards[i];
            cardParentObject.AddComponent<BoxCollider>().size = new Vector3(CardMeshCreator.Instance.cardMeshWidth, CardMeshCreator.Instance.cardMeshHeight, 0.05f);

            GameObject particleGO = (GameObject) Instantiate(particlePrefab);
            particleGO.transform.SetParent(cardParentObject.transform);
            particleGO.transform.localPosition = new Vector3(0,0,0.2f);

            GameObject cardGO = CardMeshCreator.Instance.CreateCard(deck.cards[i]);
            cardGO.transform.SetParent(cardParentObject.transform);
            cardGO.transform.position = cardGO.GetComponent<MeshFilter>().mesh.bounds.extents * -1f;

            cardParentObject.transform.position = new Vector3(Random.Range(-0.05f,0.05f), 0, i * 0.015f);
            cardParentObject.transform.Rotate(new Vector3(0, 0, 0));
            CardGameObjectDict.Add(deck.cards[i], cardParentObject);
            GameObjectCardDict.Add(cardParentObject, deck.cards[i]);
        }

        cardGameObjects_container.transform.Translate(0,2.5f,0.5f);
        cardGameObjects_container.transform.eulerAngles = new Vector3(320,0,0);
    }

    public void AddCardToHand()
    {
        Card cardData = deck.DrawCard();
        HandManager.Instance.AddCardToHand(cardData);
    }


    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
