﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

    private static InputManager _instance;
    public static InputManager Instance { get { return _instance; } }

    public delegate void CardInputHandler(GameObject cardGO);
    //public event CardInputHandler CardClicked;
    public event CardInputHandler DragStarted;
    public event CardInputHandler DragEnded;

    public delegate void ClickHandler();
    public event ClickHandler DeckClicked;

    private bool isDragging;
    GameObject dragGO;

    private Vector3 screenPoint;
    private Vector3 offset;



    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else {
            _instance = this;
        }
    }
	void Update () {


        if (isDragging)
        {
            if (Input.GetMouseButton(0)) {
                Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
                Vector3 curPosition =   Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
                curPosition.x = Mathf.Clamp(curPosition.x, -4f, 4f);
                curPosition.z = Mathf.Clamp(curPosition.z, -2f, 1f);
                curPosition.y += 0.11f;
                dragGO.transform.position = curPosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                isDragging = false;
                if (DragEnded != null) DragEnded(dragGO);
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit && hitInfo.transform.GetComponent<CardInfo>() != null)
            {
                CardInfo cardInfo = hitInfo.transform.GetComponent<CardInfo>();
                if (cardInfo.interactable)
                {
                    //if (CardClicked != null) CardClicked(hitInfo.transform.gameObject);
                    isDragging = true;
                    dragGO = hitInfo.transform.gameObject;

                    screenPoint = Camera.main.WorldToScreenPoint(dragGO.transform.position);

                    offset = dragGO.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

                    if (DragStarted != null) DragStarted(hitInfo.transform.gameObject);
                }
            } else if(hit && hitInfo.transform.name == "Cards Container")
            {
                GameManager.Instance.AddCardToHand();
                if (DeckClicked != null) DeckClicked();
            }
        }

                
    }

        
}

