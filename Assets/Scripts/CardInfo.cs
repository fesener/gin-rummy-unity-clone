﻿using UnityEngine;
using System.Collections;

public class CardInfo : MonoBehaviour {

    public bool interactable;
    public Card card;
    public bool inHand;

}
