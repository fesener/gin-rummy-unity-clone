﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Xml;
using System;




public class CardMeshCreator: MonoBehaviour{

    public Material frontMaterial;
    public Material backMaterial;

    float textureAtlasWidth;
    float textureAtlasHeight;

    public TextAsset textureAtlasXML;

    private static CardMeshCreator _instance;
    public static CardMeshCreator Instance
    {
        get
        {
            return _instance;
        }
    }

    Dictionary<string, TextureItem> textureDict;

    public float cardMeshWidth = 0.8f;
    public float cardMeshHeight = 1.2f;


    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else {
            _instance = this;
        }
        createTextureDictionary();
    }


public void createTextureDictionary()
{
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(textureAtlasXML.text);
       
        XmlNodeList nodes = xmldoc.SelectNodes("TextureAtlas/SubTexture");
        textureDict = new Dictionary<string, TextureItem>();

        foreach (XmlNode node in nodes)
        {
            TextureItem textureItem = new TextureItem();
            textureItem.name = node.Attributes.GetNamedItem("name").Value;
            int.TryParse(node.Attributes.GetNamedItem("x").Value, out textureItem.x);
            int.TryParse(node.Attributes.GetNamedItem("y").Value, out textureItem.y);
            int.TryParse(node.Attributes.GetNamedItem("width").Value, out textureItem.width);
            int.TryParse(node.Attributes.GetNamedItem("height").Value, out textureItem.height);

            textureDict.Add(node.Attributes.GetNamedItem("name").Value, textureItem);

            textureAtlasHeight = frontMaterial.mainTexture.height;
            textureAtlasWidth = frontMaterial.mainTexture.width;
        }
    }



    public Vector2[] GenerateUVArray(TextureItem textureItem)
    {

        textureItem.y = (int)textureAtlasHeight - textureItem.y - textureItem.height;

        Vector2[] uv = new Vector2[8];


        uv[0] = new Vector2(textureItem.x / textureAtlasWidth, textureItem.y / textureAtlasHeight);
        uv[1] = new Vector2((textureItem.x + textureItem.width) / textureAtlasWidth, textureItem.y / textureAtlasHeight);
        uv[2] = new Vector2(textureItem.x / textureAtlasWidth, (textureItem.y + textureItem.height) / textureAtlasHeight);
        uv[3] = new Vector2((textureItem.x + textureItem.width) / textureAtlasWidth, (textureItem.y + textureItem.height) / textureAtlasHeight);


        textureItem = textureDict["cardBack.png"];
        textureItem.y = (int)textureAtlasHeight - textureItem.y - textureItem.height;

        uv[4] = new Vector2(textureItem.x / textureAtlasWidth, textureItem.y / textureAtlasHeight);
        uv[5] = new Vector2((textureItem.x + textureItem.width) / textureAtlasWidth, textureItem.y / textureAtlasHeight);
        uv[6] = new Vector2(textureItem.x / textureAtlasWidth, (textureItem.y + textureItem.height) / textureAtlasHeight);
        uv[7] = new Vector2((textureItem.x + textureItem.width) / textureAtlasWidth, (textureItem.y + textureItem.height) / textureAtlasHeight);


        return uv;
    }

    public GameObject CreateCard(Card cardData)
    {
        GameObject card_GO = new GameObject();
        card_GO.name = cardData.ToString();
        MeshFilter card_GO_meshFilter = card_GO.AddComponent<MeshFilter>();
        MeshRenderer card_GO_meshRenderer = card_GO.AddComponent<MeshRenderer>();
        Mesh card_GO_mesh = new Mesh();
        card_GO_meshFilter.mesh = card_GO_mesh;

        Vector2[] uvs = GenerateUVArray(textureDict["card"+cardData.Suit+""+(int)cardData.Value+".png"]);


        Vector3[] vertices = new Vector3[8]
        {
            new Vector3(0,0,0),
            new Vector3(cardMeshWidth,0,0),
            new Vector3(0,cardMeshHeight,0),
            new Vector3(cardMeshWidth, cardMeshHeight,0),
            
            new Vector3(0,0,0),
            new Vector3(cardMeshWidth,0,0),
            new Vector3(0,cardMeshHeight,0),
            new Vector3(cardMeshWidth, cardMeshHeight,0)
        };

        int[] front_triangles = new int[6]
        {
            0,
            2,
            1,
            2,
            3,
            1
        };

        int[] back_triangles = new int[6]
        {
            5,
            6,
            4,
            5,
            7,
            6
        };


        Vector3[] normals = new Vector3[8]
        {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            Vector3.forward,
            Vector3.forward,
            Vector3.forward,
            Vector3.forward
        };

        card_GO_mesh.vertices = vertices;
        card_GO_mesh.normals = normals;

        card_GO_mesh.subMeshCount = 2;
        card_GO_mesh.SetTriangles(front_triangles, 0);
        card_GO_mesh.SetTriangles(back_triangles, 1);

        
        card_GO_mesh.uv = uvs;

        card_GO_meshRenderer.sharedMaterials = new Material[]
        {
            frontMaterial,
            backMaterial
        };

        return card_GO;



    }
}

public struct TextureItem
{
    public string name;
    public int x;
    public int y;
    public int width;
    public int height;
}



